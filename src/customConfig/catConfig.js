export const Category = [
    {
        name: '1',
        subCat: [{ name: '1' }, { name: '2' }, { name: '3' }, { name: '4' }, { name: '5' }, { name: '6' }]
    },
    {
        name: '2',
        subCat: [{ name: '三餐' }, { name: '零食' }, { name: '饮料' }, { name: '水果' }, { name: '外卖' }, { name: '食材' }]
    },
    {
        name: '3',
        subCat: [{ name: '房租' }, { name: '房贷' }, { name: '水电煤' }, { name: '通讯费' }, { name: '网费' }, { name: '物业' }, { name: '家具电器' }]
    },
    {
        name: '4',
        subCat: [{ name: '上下班' }, { name: '地铁' }, { name: '车辆油费与保养' }, { name: '火车' }, { name: '轮船' }, { name: '飞机' }]
    },
    {
        name: '5',
        subCat: [{ name: '洗漱用品' }, { name: '餐具杯具' }, { name: '厨房用品' }, { name: '卫生纸' }, { name: '其他' }]
    },
    {
        name: '6',
        subCat: [{ name: '化妆品' }, { name: '女生用品' }]
    }


]
